<?php

function timeToNum($time) {
	$number = str_replace(':', '.', $time);
	$number = number_format((float)$number, 2, '.', '');

	return $number;
}

function formatTime($time) {
	$formatted = '';

	if (strpos(':', $time) !== 'false') {
		$arTime = explode(':', $time);

		if ($arTime) {
			if ($arTime[0] != 00) $formatted .= $arTime[0] . '<span>ч</span> ';
			if ($arTime[1] != 00) $formatted .= $arTime[1] . '<span>м</span> ';
		}

	} else {
		$formatted = $time . 'ч';
	}

	return $formatted;
}

function timePercent($num1, $num2) {
	$res = 0;
	$num1_sec = timeToSeconds($num1);
	$num2_sec = timeToSeconds($num2);

	$res = 100 - ( $num1_sec * 100 / $num2_sec );

	return $res;
}

function timeToSeconds($num1) {
	$hours = $minutes = $seconds = 0;
	sscanf($num1, "%d:%d:%d", $hours, $minutes, $seconds);

	$time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 3600 + $minutes * 60;

	return $time_seconds;
}

function string_replace($string, $arReplace) {
	if (is_array($arReplace)) {
		foreach($arReplace as $key => $value) {
			str_replace($key, $value, $string);
		}
	}

	return $string;
}