<?php
namespace Filters;

use Illuminate\Session\Store;

class ViewThrottleFilter {
	private $session;

	public function __construct(Store $session) {
		$this->session = $session;
	}

	public function filter() {
		$reviews = $this->getViewedReviews();

		if ( ! is_null($reviews)) {
			$reviews = $this->cleanExpiredViews($reviews);

			$this->storeReviews($reviews);
		}
	}

	private function getViewedReviews() {
		return $this->session->get('viewed_reviews', null);
	}

	private function cleanExpiredViews($reviews) {
		$time = time();

		$throttleTime = 3600;

		return array_filter($reviews, function ($timestamp) use ($time, $throttleTime) {
			return ($timestamp + $throttleTime) > $time;
		});
	}

	private function storeReviews($reviews) {
		$this->session->put('viewed_reviews', $reviews);
	}
}