<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 02.12.2014
 * Time: 12:16
 */

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Review extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	use Conner\Tagging\TaggableTrait;

	public function user() {
		return $this->hasOne('User', 'id', 'editorId');
	}

	public function category() {
		return $this->hasOne('Category', 'id', 'categoryId');
	}

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'reviews';

	protected $fillable = array('editorId', 'title', 'reviewScreenshot', 'thumbScreenshot', 'reviewUrl',
	'categoryId', 'gameName', 'description', 'reviewText',
	'hoursPlayed', 'hoursWasted', 'isSpoilers', 'isPublished',
	'storyScore', 'controlScore', 'graphicsScore', 'tags',
	'hoursScore', 'allScore', 'soundScore',
	'published_at', 'editorsAward');

	public static $validation = array();

	public function register() {
		$this->save();
		return $this->id;
	}
} 