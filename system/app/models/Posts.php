<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 02.12.2014
 * Time: 12:16
 */

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Posts extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	use Conner\Tagging\TaggableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'lolposts';

	protected $fillable = array(
		'title',
		'gamename',
		'image',
		'description',
		'url',
		'likes',
		'longpost'
	);

	public static $validation = array(
		'tags' => 'required'
	);

	public function register() {
		$this->save();
		return $this->id;
	}
}
