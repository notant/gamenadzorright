<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Category extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	public function review() {
		return $this->hasMany('Review', 'categoryId');
	}

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	protected $fillable = array('categoryName', 'categoryUrl', 'categoryPriority', 'categoryShowInMenu');

	public static $validation = array(
		// Поле username является обязательным, содержать только латинские символы и цифры, и
		// также быть уникальным в таблице users
		'categoryName'  => 'required',
	);

	public function register() {
		$this->save();
	}

	public function scopeSelect($query, $title = '') {
		$selectVals[''] = $title;
		$selectVals += $this->lists('categoryName', 'id');
		return $selectVals;
	}
}