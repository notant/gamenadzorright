<?php
namespace Events;

use Review;
use Illuminate\Session\Store;

class ViewReviewHandler {

	private $session;

	public function __construct(Store $session) {
		$this->session = $session;
	}

	public function handle(Review $review)
	{
		if (!$this->isPostViewed($review)) {
			$review->increment('view_count');

			$this->storeReview($review);
		}
	}

	private function isPostViewed($review) {

		$viewed = $this->session->get('viewed_reviews', array());

		return array_key_exists($review->id, $viewed);

	}

	private function storeReview($review)
	{

		$key = 'viewed_reviews.' . $review->id;

		$this->session->put($key, time());
	}
}