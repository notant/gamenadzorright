<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	public function reviews() {
		return $this->hasMany('Review', 'editorId');
	}

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	protected $fillable = array('username', 'profilename', 'password', 'userimage');

	public static $validation = array(
		// Поле username является обязательным, содержать только латинские символы и цифры, и
		// также быть уникальным в таблице users
		'username'  => 'required|alpha_dash|unique:users',

		// Поле password является обязательным, должно быть длиной не меньше 6 символов, а
		// также должно быть повторено (подтверждено) в поле password_confirmation
		'password'  => 'required|confirmed|min:6',
	);

	public function register() {
		$this->password = Hash::make($this->password);
		$this->activationCode = $this->generateCode();
		$this->isActive = true;
		$this->save();
//
		return $this->id;
	}

	protected function generateCode() {
		return Str::random(); // По умолчанию длина случайной строки 16 символов
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}
	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}
}
