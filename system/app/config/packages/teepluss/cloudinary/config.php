<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Cloudinary API configuration
	|--------------------------------------------------------------------------
	|
	| Before using Cloudinary you need to register and get some detail
	| to fill in below, please visit cloudinary.com.
	|
	*/

	'cloudName'  => 'gamenadzor-ru',
	'baseUrl'    => 'http://res.cloudinary.com/gamenadzor-ru',
	'secureUrl'  => 'https://res.cloudinary.com/gamenadzor-ru',
	'apiBaseUrl' => 'https://api.cloudinary.com/v1_1/gamenadzor-ru',
	'apiKey'     => '562825637767964',
	'apiSecret'  => 'AK2qWVQ55gSN7osRqd0HmEOsQgk',

	/*
	|--------------------------------------------------------------------------
	| Default image scaling to show.
	|--------------------------------------------------------------------------
	|
	| If you not pass options parameter to Cloudy::show the default
	| will be replaced.
	|
	*/

	'scaling'    => array(
		'format' => 'jpg',
		'width'  => 280,
		'height' => 145,
		'crop'   => 'fit',
		'effect' => null
	)

);