<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Upload dir
    |--------------------------------------------------------------------------
    |
    | The dir where to store the images (relative from public)
    |
    */

    'dir' => 'upload/elfinder',

    /*
    |--------------------------------------------------------------------------
    | Access filter
    |--------------------------------------------------------------------------
    |
    | Filter callback to check the files
    |
    */

    'access' => 'Barryvdh\Elfinder\Elfinder::checkAccess',

    /*
    |--------------------------------------------------------------------------
    | Roots
    |--------------------------------------------------------------------------
    |
    | By default, the roots file is LocalFileSystem, with the above public dir.
    | If you want custom options, you can set your own roots below.
    |
    */

    'roots' => null,
    
    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | These options are merged, together with 'roots' and passed to the Connector.
    | See https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options-2.1
    |
    */

    'options' => array(
		'bind'   => array(
			/* regist action, require */
			'upload.presave' => array(
				'Plugin.Watermark.onUpLoadPreSave',
				'Plugin.AutoResize.onUpLoadPreSave'
			)
		),
		'plugin' => array(
			/* optional, default values are */
			'Watermark' => array(
				'source'         => 'watermark.png', // Path to Water mark image
				'marginRight'    => 25,          // Margin right pixel
				'marginBottom'   => 25,          // Margin bottom pixel
				'quality'        => 90,         // JPEG image save quality
				'transparency'   => 70, // Water mark image transparency
				// ( other than PNG )
				'targetType'     => IMG_GIF|IMG_JPG|IMG_PNG|IMG_WBMP,
				// Target image formats ( bit-field )
				'targetMinPixel' => 200         // Target image minimum
				// pixel size
			),
			'PluginAutoResize' => array(
				'enable'         => true,       // For control by volume driver
				'maxWidth'       => 1024,       // Path to Water mark image
				'maxHeight'      => 1024,       // Margin right pixel
				'quality'        => 100,         // JPEG image save quality
				'targetType'     => IMG_GIF|IMG_JPG|IMG_PNG|IMG_WBMP // Target image formats ( bit-field )
			)
		),
	),

    /*
    |--------------------------------------------------------------------------
    | CSRF
    |--------------------------------------------------------------------------
    |
    | CSRF in a state by default false.
    | If you want to use CSRF it can be replaced with true (boolean).
    |
    */

    'csrf'=>null,

);
