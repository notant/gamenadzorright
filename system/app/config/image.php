<?php

return array(
	'library'     => 'imagick',
	'upload_dir'  => 'upload',
	'upload_path' => public_path() . '/upload/',
	'quality'     => 85,
);