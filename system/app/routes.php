<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|


*/
// usage inside a laravel route

Route::get('/', 'CMain@getMain');

Route::get('reviews/{category}/{review}', 'CReviews@getReview');

//Route::get('aboutus', function() {
//
//});
//
//Route:;get('faq', function() {
//
//});

Route::resource('reviews', 'CReviews',
	['only' => ['index', 'show']]);

Route::when('*', 'reviews.view_throttle');

Route::controller('api', 'APIController');

Route::controller('users', 'UsersController');

Route::get('search', array('as' => 'search.index', 'uses' => 'SearchController@getIndex'));

Route::get('elfinder/tinymce', 'Barryvdh\Elfinder\ElfinderController@showTinyMCE4');
\Route::get('elfinder/ckeditor4', 'Barryvdh\Elfinder\ElfinderController@showCKeditor4');
Route::group(array('before' => 'auth'), function()
{
	\Route::get('elfinder', 'Barryvdh\Elfinder\ElfinderController@showIndex');
	\Route::any('elfinder/connector', 'Barryvdh\Elfinder\ElfinderController@showConnector');
});

Route::group(array('prefix' => 'gn-admin'), function()
{

	Route::get('/',
		array(
			'as' => 'admin.main',
			function() {
				return Redirect::route('dashboard.login');
			}
		)
	);

	Route::get(
		'register-editor',
		array(
			'as' => 'admin.reg',
			function() {
				return View::make('gn-admin.register');
			}
		)
	);

	Route::post(
		'register-editor',
		'AdminController@regEditor'
	);

	Route::get(
		'profile',
		array(
			'as' => 'admin.profile',
			'uses' => 'AdminController@getProfile',
		)
	)->before('auth');

	Route::get('login', array('as' => 'dashboard.login', 'uses' => 'Dashboard@getLoginIndex'));
	Route::post('login', 'Dashboard@postLoginUser');

	Route::post('profile/{id}/settings', array('uses' => 'AdminController@postProfile'));
	Route::post('profile/{id}/image', array('uses' => 'AdminController@postImage'));

	Route::group(array('before' => 'auth'), function() {

		Route::controller('reviews', 'CAdminReviews');
		Route::controller('categories', 'CAdminCategories');
		Route::controller('posts', 'CAdminDashboard');

		Route::group(array('prefix' => 'dashboard'), function() {
			Route::get('/', 'Dashboard@getIndex');

			Route::get('/manage/categories/all', 'Dashboard@getAllCategories');
			Route::get('/manage/categories/get/{id?}', 'Dashboard@getEditableCategory');
			Route::post('/manage/categories/delete/{id}', 'Dashboard@deleteCategory');

			Route::get('/manage/reviews/all', 'Dashboard@getAllReview');
			Route::get('/manage/reviews/get/{id?}', 'Dashboard@getEditableReview');

			Route::get('/manage/lols/all', 'Dashboard@getAllLols');
			Route::get('/manage/lols/get/{id?}', 'Dashboard@getEditableLol');
		});

	});

});

Route::get('/logout', function() {
	Auth::logout();
	Session::flush();
	return Redirect::to('/');
});

Route::get('sitemap.xml', function(){

	$sitemap = App::make("sitemap");

	$sitemap->setCache('gamenadzor.sitemap', 3600);

	if (!$sitemap->isCached())
	{
		$sitemap->add(URL::to('/'), date('c',time()), '1.0', 'daily');

		$category = Category::all();

		foreach($category as $cat) {
			$sitemap->add(URL::action('CReviews@show', $cat->categoryUrl), $cat->created_at, 0.7, 'daily');

			foreach ($cat->review as $rev) {
				if ($rev->isPublished == 1) {
					$url = URL::action('CReviews@getReview', array('category' => $cat->categoryUrl, $rev->reviewUrl));
					$date = $rev->created_at;
					$sitemap->add($url, $date, 0.8, 'daily');
				}
			}
		}

	}

	return $sitemap->render('xml');
});

Route::post('vk-share', function() {

	$arr = array(
		'access_token' => 'b7dd957385075dea4178c4ff89301ec5dc8364bd1c7205011ad9a045b44e26aaf96b9f22bae5d19df45d8',
		'expires_in' => 0,
		'user_id' => 227944286,
		'created' => time()
	);

	Soc::setAccessToken(json_encode($arr));

	$c_sid = Session::get('captcha_sid');
	$c_key = Input::get('vk_captcha');
	$captcha = array(
		'captcha_sid' => $c_sid,
		'captcha_key' => $c_key
	);

	$url = URL::action('CReviews@getReview', array(
		'category' => $_POST['category']['categoryUrl'],
		'review' => $_POST['reviewUrl']
	));

	$result = Soc::postToPublic('83333026',
		$_POST['description'],
		$_POST['reviewScreenshot'],
		$url,
		$captcha,
		explode(', ', $_POST['tags'])
	);

	return Response::json($result);
});