<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GnGalleryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lolposts', function(Blueprint $table) {

			$table->increments('id');

			/**
			 * title
			 * image
			 * description
			 * tags
			 * url
			 * views
			 * likes
			 * gamename
			 *
			 */

			$table->string('title', 240);
			$table->text('gamename');
			$table->text('image');
			$table->text('description');
			$table->text('tags');
			$table->text('url');
			$table->integer('views');
			$table->integer('likes');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lolposts');
	}

}
