<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrReviews extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reviews', function(Blueprint $table) {

			$table->increments('id');

			$table->integer('categoryId')->references('id')->on('categories');

			$table->integer('editorId')->references('id')->on('users');

			$table->string('title');

			$table->string('gameName');

			$table->string('description');

			$table->longText('reviewText');

			$table->time('hoursPlayed');

			$table->time('hoursWasted');

			$table->boolean('isSpoilers');

			$table->boolean('isPublished');

			$table->double('storyScore');

			$table->double('controlScore');

			//$table->double('');

			$table->longText('tags');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reviews');
	}

}
