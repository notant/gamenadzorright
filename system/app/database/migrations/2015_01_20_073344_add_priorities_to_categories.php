<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrioritiesToCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categories', function($table) {
			$table->integer('categoryPriority')->after('categoryUrl');
			$table->boolean('categoryShowInMenu')->after('categoryPriority');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categories', function($table)	{
			$table->dropColumn('categoryPriority');
			$table->dropColumn('categoryShowInMenu');
		});
	}

}
