<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewRatingFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reviews', function($table) {
			$table->double('soundScore')->after('graphicsScore')->nullable();
			$table->double('allScore')->after('soundScore');
			$table->double('hoursScore')->after('hoursWasted');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reviews', function($table) {
			$table->dropColumn('soundScore');
			$table->dropColumn('allScore');
			$table->dropColumn('hoursScore');
		});
	}

}
