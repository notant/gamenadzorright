<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewurlToReviews extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reviews', function($table) {
			$table->string('reviewUrl')->after('title');
			$table->double('graphicsScore')->after('controlScore');
			$table->text('reviewScreenshot')->after('title')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reviews', function($table) {
			$table->dropColumn('reviewUrl');
			$table->dropColumn('graphicsScore');
			$table->dropColumn('reviewScreenshot');
		});
	}

}
