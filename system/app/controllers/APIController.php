<?php

class APIController extends BaseController {

	public function postUpload() {
		$destinationPath = 'upload/' . Auth::user()->id . '/' . date('Ymd') . '_' . Auth::user()->username . '/';

		if(Input::hasFile('images')) {

			$uploadSuccess = false;
			$uploadedFiles = array();

			$file = Input::file('images'); // your file upload input field in the form should be named 'file'

			// Declare the rules for the form validation.
			$rules = array('images'	=> 'mimes:jpg,jpeg,bmp,png');
			$data = array('images' 	=> Input::file('myfile'));

			// Validate the inputs.
			$validation = Validator::make($data, $rules);

			if ($validation->fails()) {
				return Response::json('error', 400);
			}

			if(is_array($file)) {
				foreach($file as $part) {
					$uploadedFiles[] = $this->uploadImage($part, $destinationPath);
				}
			}
			else {
				$uploadedFiles[] = $this->uploadImage($file, $destinationPath, true);
			}

			if( count($uploadedFiles) > 0 ) {
				return Response::json(array('files' => $uploadedFiles, 'success' => 200));
			} else {
				return Response::json('error', 400);
			}
		}
	}

	protected function uploadImage($file, $pathForUpload, $resize = false) {
		$filename = $file->getClientOriginalName();
		$uploadSuccess = Input::file('images')->move($pathForUpload, $filename);

		if ($uploadSuccess) {
			$folder = '/' . $pathForUpload . $filename;

			if ($resize) {
				$this->saveResized(null, 250, $filename, $pathForUpload);
			}

			return $folder;
		}

		return '';
	}

	protected function saveResized($w = null, $h, $filename, $path) {
		$image = Image::make($path . $filename);

		if (!$w) {
			$image->heighten($h);
			$w = 'auto';
		} else {
			$image->resize($w, $h);
		}

		$resizeFolder = "{$path}/resize";
		if (!is_dir($resizeFolder)) {
			mkdir($resizeFolder, 0777);
		}

		$resizedPath = public_path() . '/' . "{$resizeFolder}/{$w}_{$h}_{$filename}";

		$image->save($resizedPath);
	}

	public function LikesWidgetIncrement($id) {
		$post = Posts::find($id);

		if ($post) {
			$number = $post->likes;
			$post->update(
				array('likes' => $number++)
			);
		}
	}

}