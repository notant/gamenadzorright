<?php

class UsersController extends BaseController {

	public function getRegistration() {

		return View::make('users.registration');
	}

	public function postRegistration() {
		// Проверка входных данных
		$rules = User::$validation;
		$validation = Validator::make(Input::all(), $rules);
		if ($validation->fails()) {
			// В случае провала, редиректим обратно с ошибками и самими введенными данными
			return Redirect::to('users/registration')->withErrors($validation)->withInput();
		}

		$user = new User();
		$user->fill(Input::all());

		if (Input::hasFile('userimage')) {
			$file            = Input::file('userimage');
			$destinationPath = 'upload/p/';
			$filename        = date('Ymd') . '_' . $file->getClientOriginalName();
			$uploadSuccess   = $file->move($destinationPath, $filename);
			$user->fill(array('userimage' => $uploadSuccess));
		}

		$id = $user->register();

		if (
			Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')), true)
			&& View::exists('users.profile')
			) {
			return Redirect::to('users/profile');
		} else {
			return Redirect::to('/');
		}
	}


	public function getProfile() {
		return View::make('users.profile');
	}
}