<?php

class CReviews extends BaseController {

	public function getShow($categoryName) {
		if (Review::find($categoryName)) {
		//	print_r("hello");
		}
	}

	public function index() {
		$reviews = Review::where('isPublished', '=', 1)
					->whereNotNull('reviewUrl')
					->whereNotIn('categoryId', array(0, 27))
					->orderBy('created_at', 'desc')
					->paginate(10);

		return View::make('site.reviews')->with('reviews', $reviews);
	}

	public function show($categoryName) {
		$category = Category::where('categoryUrl', '=', $categoryName)->firstOrFail();

		$title = "{$category->categoryName}";

		$reviews = Review::where('categoryId', '=', $category->id)
			->where('isPublished', '=', 1)
			->whereNotNull('reviewUrl')
			->orderBy('created_at', 'desc')
			->paginate(10);

		return View::make('site.reviews')->with(
			array(
				'reviews' => $reviews,
				'title' => $title
			)
		);
	}

	public function getReview($category, $review) {
		$cat = Category::where('categoryUrl', '=', $category)->first();
		if (!$cat) {
			App::abort(404);
		}

		$rev = Review::where('reviewUrl', '=', $review)
			->where('isPublished', '=', '1')
			->where('categoryId', '=', $cat->id)
			->first();

		if (!$rev) {
			App::abort(404);
		}

		Event::fire('reviews.view', $rev);

		$keywords = ', ' . $rev->tags;
		$title = "{$cat->categoryName} | {$rev->title}";
		$desc = trim(str_limit($rev->description, 100, '...'));

		$tags = explode(',', $rev->tags);

		return View::make('site.review')->with(
			array(
				'keywords' => $keywords,
				'title' => $title,
				'review' => $rev,
				'desc' => $desc,
				'tags' => $tags
			)
		);
	}

}