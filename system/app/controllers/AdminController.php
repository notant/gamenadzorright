<?php

class AdminController extends BaseController {

	public function getLogin() {
		if (Auth::check()) {
			return Redirect::to('gn-admin/profile');
		}
		return View::make('gn-admin.login');
	}

	public function loginUser() {
		if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')), true)) {
			if (Auth::user()->isEditor || Auth::user()->isAdmin) {
				return Redirect::to('gn-admin/profile');
			} else {
				Auth::logout();
				return Redirect::to('/')->with('message', 'U no admin');
			}
		} else {
			return Redirect::to('gn-admin/login')->with('message', 'Такая комбинация логина/пароля не найдена');
		}

	}

	public function regEditor() {
		// Проверка входных данных
		$rules = User::$validation;
		$validation = Validator::make(Input::all(), $rules);

		if ($validation->fails()) {
			// В случае провала, редиректим обратно с ошибками и самими введенными данными
			return Redirect::to('gn-admin/register')->withErrors($validation)->withInput();
		}

		$user = new User();
		$user->fill(Input::all());

		if (Input::hasFile('userimage')) {
			$file            = Input::file('userimage');
			$destinationPath = 'upload/p/';
			$filename        = str_random(6) . '_' . $file->getClientOriginalName();
			$uploadSuccess   = $file->move($destinationPath, $filename);
			$user->fill(array('userimage' => $uploadSuccess));
		}

		$id = $user->register();

		if (Auth::user()) {

			if (Auth::user()->id != $id) {
				Auth::logout();
			}

		}

		return Redirect::to('gn-admin/profile');

	}

	public function getProfile() {

		$reviews = Review::where('editorId', '=', Auth::user()->id)
					->orderBy('isPublished', 'asc')
					->get();

		return View::make('gn-admin.profile')->with(
			array(
				'reviews' => $reviews
			)
		);
	}

	public function postProfile($id) {
		$user = User::find($id);

		$validation = Validator::make(Input::all(), array(
			'password' => 'confirmed|min:6',
		));

		if ($validation->passes()){
			if (Input::has('profilename'))
				$user->update(array('profilename' => Input::get('profilename')));

			$user->update(array('password' => Hash::make(Input::get('password'))));
		}

		return Redirect::to('gn-admin/profile')->withErrors($validation)->withInput();
	}

	public function postImage($id) {
		$user = User::find($id);

		if (Input::hasFile('userimage')) {
			$file            = Input::file('userimage');
			$destinationPath = 'upload/p/';
			$filename        = str_random(6) . '_' . $file->getClientOriginalName();
			$original   = $file->move($destinationPath, $filename);

			$main = Image::resize($original, 1200, 500);
			$user->update(array('userimage' => $main));
		}

		return Redirect::to('gn-admin/profile');
	}

}