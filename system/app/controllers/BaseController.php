<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	protected function getMessage($message, $redirect = false) {
		return View::make('message', array(
			'message'   => $message,
			'redirect'  => $redirect,
		));
	}

	/**
	 * @param      $string
	 * @param bool $lower
	 *
	 * @return string
	 */
	protected function getTranslate($string, $lower = true) {

		$string = Mascame\Urlify::transliterate( $string );
		$string = \Illuminate\Support\Str::slug($string);

		return ($lower) ? strtolower($string) : $string;
	}

}
