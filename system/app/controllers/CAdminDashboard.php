<?php

class CAdminDashboard extends BaseController {

	public function getLol($id = null) {
		$lolpost = Posts::find($id);
		$allTagsSelect = array();
		$tagsSelected = array();

		foreach(Conner\Tagging\Tag::all() as $tag) {
			$allTagsSelect[$tag->name] = $tag->name;
		};

		if ($lolpost) {
			foreach($lolpost->tagNames() as $tag) {
				$tagsSelected[] = $tag;
			}
		}

		return View::make('gn-admin.lolnadzor.createLOL')
			->with(
				array(
					'lolpost' => $lolpost,
					'allTagsSelect' => $allTagsSelect,
					'tagsSelected' => $tagsSelected,
				)
			);
	}

	public function getAllLols() {
		$posts = Posts::all();

		return View::make('gn-admin.lolnadzor.lols')->with('posts', $posts);
	}

	public function postCreateLol() {
		$response = array();
		$rules = Posts::$validation;
		$validation = Validator::make(Input::all(), $rules);
		$post = new Posts();

		if ($validation->fails()) {
			$messages = $validation->messages();
			$response = array(
				'result' => false,
				'reason' => 'validation-failed',
				'errors' => $messages,
			);

			return Redirect::action('CAdminDashboard@getLol')->withErrors($validation);
		}

		if ($validation->passes()) {
			$post->fill(Input::all());

			if (Input::hasFile('image')) {
				$file = Input::file('image');

				$serverPath 		= '/home/helpmewi/subdomains/static/images';
				$lolFilePath 		= '/public/lol/' . date('Ymd') . '/' . str_random(2) . '/';
				$destinationPath	= $serverPath . $lolFilePath;
				$filename			= str_replace(' ', '_', $file->getClientOriginalName());
				$filePath 			= $destinationPath . $filename;
				$original			= $file->move($destinationPath, $filename);

				if ($original) {
					$post->fill(
						array('image' => '//static.images.gamenadzor.ru' . $lolFilePath . $filename)
					);
				}
			}

			$id = $post->register();
			if ($id) {
				$response = array(
					'result' => $id,
				);

				$post->update(
					array('url' => $id . '/' . $this->getTranslate(Input::get('title')))
				);

				$post->tag(Input::get('tags'));

				return Redirect::action('CAdminDashboard@getLol', array($id));
			}
		}

		return Redirect::action('CAdminDashboard@getLol');
	}

	public function postEditLol() {
		$id = Input::get('lolID');
		$rules = Posts::$validation;
		$validation = Validator::make(Input::all(), $rules);
		$post = Posts::find($id);

		if ($validation->fails()) {
			$messages = $validation->messages();
			$response = array(
				'result' => false,
				'reason' => 'validation-failed',
				'errors' => $messages,
			);

			return Redirect::action('CAdminDashboard@getLol')->withErrors($validation);
		}

		if ($validation->passes()) {
			$image = $post->image;

			$post->update(Input::all());

			if (!Input::has('longpost')) {
				$post->update(array('longpost' => false));
			}
			if ($image) {
				$post->update(array('image' => $image));
			}

			$post->retag(Input::get('tags'));

			return Redirect::action('CAdminDashboard@getLol');
		}
	}

	public function postDeleteLol($id) {
		if (!$id) return Response::json('Can not do it!');

		$post = Posts::find($id);

		if ($post) {
			$post->untag();

			return Response::json(
				array(
					'result' => $post->delete(),
				)
			);
		}
	}

}
