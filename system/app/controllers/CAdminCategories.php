<?php

class CAdminCategories extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('gn-admin.category')
			->with('categories', Category::all());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		$rules = Category::$validation;
		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {
			$category = new Category();
			$category->fill(Input::all());
			$category->fill(
				array(
					'categoryUrl' => $this->getTranslate(Input::get('categoryName'))
				));
			$category->register();

			return Redirect::to('gn-admin/categories')->with('message', 'Грац, чо.');
		} else {
			return Redirect::to('gn-admin/categories')->withErrors($validation);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	public function postEdit($id) {
		$rules = Category::$validation;
		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {
			$category = Category::find($id);

			$category->update(Input::all());

			if (Input::has('categoryName') && Input::get('categoryName') != '') {
				$category->categoryName = Input::get('categoryName');
			}

			$category->save();

			return Redirect::to('gn-admin/categories');
		}

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteDestroy($id)
	{
		Category::find($id)->delete();
		return Redirect::to('gn-admin/categories');
	}

}
