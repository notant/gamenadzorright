<?php
/**
 * Created by PhpStorm.
 * User: notant
 * Date: 29.11.14
 * Time: 23:06
 */

class CAdminReviews extends BaseController {

    public function getAddreview() {

		return View::make('parts.sup');

	}

	public function getUser($id) {

		return View::make('gn-admin.reviews.user')->with('userId', $id);
	}

	public function getCreate() {

		return View::make('gn-admin.reviews.create')->with('review', false);
	}

	public function getEdit($id) {

		$review = Review::find($id);

		return View::make('gn-admin.reviews.edit')->with('review', $review);
	}

	public function postEdit($id) {
		$response = array();
		$review = Review::find($id);

		$image = $review->reviewScreenshot;
		$reviewURL = $review->reviewUrl;
		if (!$reviewURL && Input::has('title')) {
			$review->update(
				array(
					'reviewUrl' => $this->getTranslate(Input::get('title'))
				)
			);
		}

		if ($review->isPublished == 0 && Input::get('isPublished') == 1) {
			$review->update(
				array(
					'published_at' => date('Y-m-d H:i:s')
				)
			);

			$response['published'] = true;
		}

		$review->update(Input::all());

		$inputSpoilers = Input::get('isSpoilers');
		if (!$inputSpoilers)
			$review->update(
				array('isSpoilers' => 0)
			);
		$editorsAward = Input::get('editorsAward');
		if (!$editorsAward)
			$review->update(
				array('editorsAward' => 0)
			);

		$scores = $this->calculateScores();
		$review->update(
			array(
				'hoursScore' => $scores['hours'],
				'allScore' => $scores['total']
			)
		);

		if (Input::hasFile('reviewScreenshot')) {
			$result = $this->uploadReviewScreenshot($review);
			$review->update(
				array(
					'reviewScreenshot' => $result['main'],
					'thumbScreenshot' => $result['thumb']
				)
			);
		} else {
			$review->update(array('reviewScreenshot' => $image));
		}

		Cache::forget('main_review');

		$response = array(
			'status' => 'OK',
			'msg' => 'Сохранено',
			'url' => URL::action('CReviews@getReview', array(
				'category' => $review->category['categoryUrl'],
				'review' => $review->reviewUrl
			)),
			'review' => $review,
		);

		return Response::json( $response );
	}

	public function postCreate() {
		$response = array();
		$rules = Review::$validation;
		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {

			$review = new Review();
			$review->fill(Input::all());

			if ($review->isPublished) {
				$review->fill(
					array(
						'published_at' => date('Y-m-d H:i:s')
					)
				);
			}

			if (Input::has('title')) {
				$review->fill(
					array(
						'reviewUrl' => $this->getTranslate(Input::get('title'))
					)
				);
			}

			$scores = $this->calculateScores();
			$review->fill(
				array(
					'hoursScore' => $scores['hours'],
					'allScore' => $scores['total']
				)
			);

			if (Input::hasFile('reviewScreenshot')) {
				$result = $this->uploadReviewScreenshot();
				$review->fill(
					array(
						'reviewScreenshot' => $result['main'],
						'thumbScreenshot' => $result['thumb']
					)
				);
			}

			$id = $review->register();

			Cache::forget('main_review');

			$response = array(
				'status' => 'OK',
				'msg' => 'Сохранено',
				'url' => URL::action('CReviews@getReview', array(
					'category' => $review->category['categoryUrl'],
					'review' => $review->reviewUrl
				)),
				'review' => $review,
				'published' => $review->isPublished,
				'redirect' => true,
				'redirectURL' => URL::action('CAdminReviews@getEdit', array('id' => $id)),
			);

			return Response::json( $response );
		}
	}

	protected function uploadReviewScreenshot() {
		$main = null;
		$thumb = null;

		$file = Input::file('reviewScreenshot');
		if ($file) {
			$destinationPath	= 'upload/r/' . str_random(6) . '/';
			$filename			= date('Ymd') . '_' . str_replace(' ', '_', $file->getClientOriginalName());
			$filepath 			= $destinationPath . $filename;
			$original			= $file->move($destinationPath, $filename);

			$main = Image::resize($original, 1200, 800);
			$thumb = Image::resize($original, 280, 280);
		}
		return array(
			'main' => $main,
			'thumb' => $thumb
		);
	}

	protected function calculateScores() {

		$hoursScore = doubleval( timePercent(Input::get('hoursWasted'), Input::get('hoursPlayed')) );
		$totalScore = doubleval( $this->calculateTotalScore() );

		return array(
			'hours' => number_format($hoursScore, 0, '.', ''),
			'total' => number_format($totalScore, 1, '.', '')
		);
	}

	protected function calculateTotalScore() {
		$count = 0;
		$sum = 0;

		$story = Input::get('storyScore');
		if ($this->not_null($story)) {
			$count++;
			$sum += $story;
		}

		$control = Input::get('controlScore');
		if ($this->not_null($control)) {
			$count++;
			$sum += $control;
		}

		$sound = Input::get('soundScore');
		if ($this->not_null($sound)) {
			$count++;
			$sum += $sound;
		}

		$graphics = Input::get('graphicsScore');
		if ($this->not_null($graphics)) {
			$count++;
			$sum += $graphics;
		}

//		if ($count == 0) {
//			return 0;
//		}
//		return $sum / $count;
	}

	protected function not_null($input) {
		return $input && !is_null($input) || $input > 0;
	}



} 