<?php

class Dashboard extends BaseController {

	public function getIndex() {
		return View::make('dashboard.index');
	}

	public function getAllCategories() {
		return View::make('dashboard.categories.all');
	}
	public function getEditableCategory($id = null) {
		$category = $id ? Category::find($id) : $id;

		return View::make('dashboard.categories.get')->with('category', $category);
	}
	public function postEditableCategory($id = null) {
		$action = $id ? 'update' : 'fill';
	}
	public function deleteCategory($id) {
		Category::find($id)->delete();
	}

	/**
	 * Get all reviews.
	 *
	 * @return View
	 */
	public function getAllReview() {
		return View::make('dashboard.reviews.all');
	}

	/**
	 * Get Review and Show in Dashboard.
	 * Can be Edited.
	 *
	 * If no Id is sended, show create form.
	 *
	 * @return View
	 */
	public function getEditableReview($id = null) {
		$review = null;

		if ($id) {
			$review = Review::find($id);
		}

		return View::make('dashboard.reviews.get')->with('review', $review);
	}


	public function getAllLols() {}
	public function getEditableLol($id = null) {}




	/********/
	/*
	 * LOGIN LOGIC.
	 *
	 * GET:
	 * 	LoginIndex - returns login page;
	 * POST:
	 * 	LoginUser - try to login user;
	 * 				return dashboard/index/login pages;
	 */
	/********/
	public function getLoginIndex() {
		if (Auth::check()) {
			return Redirect::to('gn-admin/profile');
		}

		$sMessage = false;
		$sClass = false;
		if (Session::has('sMessage')) {
			$sMessage = Session::get('sMessage');
			$sClass = 'form-error';
		}

		return View::make('dashboard.login')
			->with(array(
				'message' => $sMessage,
				'errorclass' => $sClass
			));
	}

	public function postLoginUser() {
		$sLogin = Input::get('username');
		$sPass = Input::get('password');
		$sMessage = '';

		if (Auth::attempt(array('username' => $sLogin, 'password' => $sPass), true)) {
			if (Auth::user()->isEditor || Auth::user()->isAdmin) {

				return Redirect::intended('/gn-admin/profile');
			} else {

				Auth::logout();
				return Redirect::action('Dashboard@getIndex');
			}
		} else {

			$sMessage = 'Такая комбинация логина/пароля не найдена';
			return Redirect::action('Dashboard@getLoginIndex')->with('sMessage', $sMessage);
		}
	}
}