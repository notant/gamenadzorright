<?php
/**
 * Created by PhpStorm.
 * User: notant
 * Date: 06.12.14
 * Time: 12:33
 */

class CMain extends BaseController {

	public function getMain() {
		$title = false;
		$pageClass = 'main-layout';

		$reviews = Review::where('isPublished', '=', '1')
			->whereNotNull('reviewUrl')
			->whereNotIn('categoryId', array(0, 27))
			->take(11)
			->orderBy('published_at', 'desc')
			->orderBy('created_at', 'desc')
			->get();


		return View::make('site.main')->with(
			array(
				'title' => $title,
				'reviews' => $reviews,
				'pageClass' => $pageClass
			)
		);
	}

} 