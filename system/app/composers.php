<?php

View::composer('layouts.master', function($view)
{
	$lastReviews = Review::take(10)
		->where('isPublished', '=', 1)
		->whereNotIn('categoryId', array(0, 27))
		->remember(360)
		->orderBy('created_at', 'desc')
		->get();

	$linkList = array();
	foreach($lastReviews as $review) {
		$linkList[] = array(
			'url' => action(
				'CReviews@getReview',
				array(
					'category' => $review->category['categoryUrl'],
					'review' => $review->reviewUrl
				)
			),
			'title' => $review->title
		);
	}


	$linkMinis = array();
	$lastMinis = Review::take(10)
		->where('isPublished', '=', 1)
		->where('categoryId', '=', 27)
		->remember(360)
		->orderBy('created_at', 'desc')
		->get();

	foreach($lastMinis as $mini) {
		$linkMinis[] = array(
			'url' => action(
				'CReviews@getReview',
				array(
					'category' 	=> $mini->category['categoryUrl'],
					'review'	=> $mini->reviewUrl
				)
			),
			'title' => $mini->title
		);
	}

	$view->with(
		array(
			'link_list'		=> $linkList,
			'link_minis'	=> $linkMinis,
		)
	);
});