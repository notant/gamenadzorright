@extends('layouts.master')

@section('content')

	@if($errors->has())
		We encountered the following errors:

		<ul>
			@foreach($errors->all() as $message)
				<li>{{ $message }}</li>
			@endforeach
		</ul>
	@endif

	{{ Form::open(array('action' => 'CAdminCategories@postCreate')) }}
		<div class="row collapse">
			<div class="large-3 columns">
				{{ Form::label('categoryName', 'Название категории', array('class' => 'prefix')) }}
			</div>
			<div class="large-6 columns">
				{{ Form::text('categoryName') }}
			</div>
			<div class="large-3 columns">
				{{ Form::submit('Добавить', array('class' => 'postfix button')) }}
			</div>
		</div>
	{{ Form::close() }}

	<table class="gn-table large-12">
		<thead>
			<tr>
				<th class="large-9">Название категории</th>
				<th class="large-3 text-center">Действия</th>
			</tr>
		</thead>
		<tbody>
	@foreach($categories as $key => $val)
		<tr>
			<td class="large-9">
				{{ Form::open(array('action' => array('CAdminCategories@postEdit', $val->id))) }}
					<div class="row collapse">
						<div class="large-2 columns">
							{{
								Form::text(
									'',
									$val->categoryName,
									array(
										'disabled' => true,
										'class' => 'prefix'
									)
								)
							}}
						</div>

						<div class="large-6 columns">
							<div class="large-12">
								{{ Form::text('categoryName', $val->categoryName) }}
							</div>

							<div class="large-12">
								<div class="large-6 columns">
								{{ Form::label('categoryPriority', 'Приоритет') }}
								{{ Form::text('categoryPriority', $val->categoryPriority) }}
								</div>
								<div class="large-6 columns">
								{{ Form::label('categoryShowInMenu', 'Показывать в меню') }}
								@if ($val->categoryShowInMenu)
									{{ Form::checkbox('categoryShowInMenu', 1, $val->categoryShowInMenu) }}
								@else
									{{ Form::checkbox('categoryShowInMenu') }}
								@endif
								</div>
							</div>
						</div>

						<div class="large-3 columns">
							{{ Form::submit('Редактировать', array('class' => 'postfix button tiny')) }}
						</div>
					</div>
				{{ Form::close() }}

			</td>
			<td class="large-3 text-center">
				{{ Form::open(array('method' => 'DELETE', 'action' =>
				array('CAdminCategories@deleteDestroy', $val->id))) }}

				{{ Form::submit('Удалить', array('class' => 'button tiny')) }}
				{{ Form::close() }}
			</td>
		</tr>
	@endforeach
		</tbody>
	</table>

@stop