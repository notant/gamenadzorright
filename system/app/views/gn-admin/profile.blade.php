@extends('layouts.master')

@section('content')

	<div class="dashboard clearfix">
		<div class="large-2 columns panel">
			<ul class="side-nav">
				<li>
					{{ HTML::linkAction('CAdminCategories@getIndex', "Категории") }}
				</li>
				<li>
					{{ HTML::linkAction('CAdminReviews@getCreate', 'Создать новый обзор') }}
				</li>
				<li class="divider"></li>
				<li>
					{{ HTML::linkAction('CAdminDashboard@getAllLols', 'Все ЛОЛы') }}
				</li>
				<li>
					{{ HTML::linkAction('CAdminDashboard@getLol', 'Создать новый ЛОЛ') }}
				</li>
				<li>
					{{ HTML::link('/logout', 'Выход') }}
				</li>
			</ul>
		</div>

		<div class="large-7 columns">
			<dl class="tabs" data-tab>
				<dd class="active"><a href="#reviews">Мои обзоры</a></dd>
				<dd><a href="#settings">Настройки</a></dd>
			</dl>
			<div class="tabs-content">
				<div class="content active" id="reviews">

					@if($reviews)

						@foreach($reviews as $review)

							<div class="panel
								@if(!$review->isPublished)
									callout
								@endif
							">
								{{ $review->title }}
								{{ HTML::link(
									URL::action('CAdminReviews@postEdit', $review->id),
									'Редактировать'
								) }}
							</div>
						@endforeach

					@endif

				</div>

				<div class="content" id="settings">
				{{ Form::open(
					array('action' => array('AdminController@postProfile', Auth::user()->id))
				) }}
					<div class="row collapse">
						<div class="large-4 columns">
							{{ Form::label('profilename', 'Имя для публикаций', array('class' => 'prefix')) }}
						</div>
						<div class="large-8 columns">
							{{ Form::text('profilename', Input::old('profilename'), array('placeholder' => 'Все увидят меня таким')) }}
						</div>
					</div>

					<div class="row collapse">
						<div class="large-4 columns">
							{{ Form::label('password', 'Пароль', array('class' => 'prefix')) }}
						</div>
						<div class="large-8 columns">
							{{ Form::password('password') }}
						</div>
					</div>


					<div class="row collapse">
						<div class="large-4 columns">
							{{ Form::label('password_confirmation', 'Подтверждение', array('class' => 'prefix')) }}
						</div>
						<div class="large-8 columns">
							{{ Form::password('password_confirmation') }}
						</div>
					</div>

					{{ Form::submit('Изменить', array('class' => 'button')) }}

				{{ Form::close() }}
				</div>
			</div>

		</div>

		<div class="large-3 columns panel">
			@if (Auth::user()->userimage)
				<img src="/{{ Auth::user()->userimage }}" />
			@endif

			{{ Form::open(
				array('action' => array('AdminController@postImage', Auth::user()->id), 'files' => true)
			) }}
				{{ Form::file('userimage') }}

				{{ Form::submit('Изменить', array('class' => 'button tiny')) }}

			{{ Form::close() }}
		</div>
	</div>


@stop