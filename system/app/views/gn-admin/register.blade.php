@extends('layouts.master')

@section('content')

	<div class="container row">

		{{ Form::open() }}

		<div class="row collapse">
			<div class="large-4 columns">
				{{ Form::label('username', 'Логин', array('class' => 'prefix')) }}
			</div>
			<div class="large-8 columns">
				{{ Form::text('username', Input::old('username'), array('placeholder' => 'myawesomelogin')) }}
			</div>
		</div>

		<div class="row collapse">
			<div class="large-4 columns">
				{{ Form::label('profilename', 'Имя для публикаций', array('class' => 'prefix')) }}
			</div>
			<div class="large-8 columns">
				{{ Form::text('profilename', Input::old('profilename'), array('placeholder' => 'Все увидят меня таким')) }}
			</div>
		</div>

		<div class="row collapse">
			<div class="large-4 columns">
				{{ Form::label('password', 'Пароль', array('class' => 'prefix')) }}
			</div>
			<div class="large-8 columns">
				{{ Form::password('password') }}
			</div>
		</div>

		<div class="row collapse">
			<div class="large-4 columns">
				{{ Form::label('password_confirmation', 'Подтверждение', array('class' => 'prefix')) }}
			</div>
			<div class="large-8 columns">
				{{ Form::password('password_confirmation') }}
			</div>
		</div>

		{{ Form::file('userimage') }}

		{{ Form::submit('Зарегистрироваться', array('class' => 'button')) }}


		{{ Form::close() }}
	</div>


@stop