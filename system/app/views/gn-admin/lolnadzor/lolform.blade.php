{{ Form::label('title') }}
{{ Form::text('title') }}
<span class="error"> {{ $errors->first('title') }}</span>

{{ Form::label('gamename') }}
{{ Form::text('gamename') }}
<span class="error"> {{ $errors->first('gamename') }}</span>

{{ Form::label('image') }}
{{ Form::file('image') }}

{{ Form::label('description') }}
{{ Form::textarea('description', null, array('size' => '30x1')) }}
<span class="error"> {{ $errors->first('description') }}</span>

{{ Form::label('tags') }}
{{ Form::text('tags') }}
<span class="error"> {{ $errors->first('tags') }}</span>