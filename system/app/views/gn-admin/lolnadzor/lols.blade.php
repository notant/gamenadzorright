@extends('layouts.master')

@section('content')

	<div class="large-12 panel">
		@foreach($posts as $post)
			<div class="lol-item" id="lol-{{$post->id}}">

				<h5>{{$post->id}}</h5>

				<p><a href="{{ URL::action('CAdminDashboard@getLol', array('id' => $post->id)) }}">Редактировать</a></p>

				{{Form::open(array('action' => array('CAdminDashboard@postDeleteLol', $post->id)))}}
					{{ Form::hidden('lolID', $post->id, array('id' => 'lolID')) }}
					{{ Form::submit('Удалить', array('class' => 'tiny button')) }}
				{{Form::close()}}

			</div>
		@endforeach
	</div>

@stop

@section('head-scripts')

	<style>
		.lol-item {
			line-height: 45px;
			border-bottom: 3px solid;
		}

		.lol-item p,
		.lol-item h5 {
			display: inline-block;
			max-width: 300px;
			float: left;
			margin: 0 15px;
			line-height: 45px;
		}
		.lol-item input {
			margin-bottom: 0;
		}
	</style>
	<script>
		$(document).ready(function() {
			$('form').on('submit', function(e) {
				var _form = $(this);

				$.ajax({
					type: 'POST',
					url: _form.attr('action'),
					data: _form.serialize,
					success: function(data) {
						if (data.result) {
							var id = _form.find('#lolID').val();
							_form.parents('#lol-' + id).remove();
						}
					}
				});

				return false;
			});
		});
	</script>

@stop