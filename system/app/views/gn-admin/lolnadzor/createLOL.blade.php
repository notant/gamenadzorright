@extends('layouts.master')

@section('content')

	<div class="large-12 panel">
	@if ($lolpost)
		{{ Form::model($lolpost, array(
								'action' => array('CAdminDashboard@postEditLol'),
								'files' => true)) }}
		{{ Form::hidden('lolID', $lolpost->id) }}
	@else
		{{ Form::open(array('action' => 'CAdminDashboard@postCreateLol', 'files' => true)) }}
	@endif

			{{ Form::label('title') }}
			{{ Form::text('title', Input::old('title')) }}

			@if($lolpost)
				@if($lolpost->image)
					<div class="lol-image">
						<img src="{{ $lolpost->image }}" />
					</div>
				@endif
			@endif

			{{ Form::label('image') }}
			{{ Form::file('image') }}

			{{ Form::label('description') }}
			{{ Form::textarea('description', null, array('size' => '30x1')) }}

			{{ Form::label('tags') }}

			@if($lolpost)
				{{ Form::select('tags[]', $allTagsSelect, $tagsSelected, array('class' => 'chosen', 'multiple' => 1)) }}
			@else
				{{ Form::select('tags[]', $allTagsSelect, false, array('class' => 'chosen', 'multiple' => 1)) }}
			@endif


			@if($errors->first('tags'))
				<span class="error"> {{ $errors->first('tags') }}</span>
			@endif

			{{ Form::label('longpost') }}
			{{ Form::checkbox('longpost') }}

		<div class="large-12 clearfix">
		@if ($lolpost)
			{{ Form::submit('Сохранить', array('class' => 'button')) }}
		@else
			{{ Form::submit('Создать', array('class' => 'button')) }}
		@endif
		</div>

		{{ Form::close() }}

		<a href="{{ URL::action('CAdminDashboard@getLol') }}">Создать новый лол</a>
	</div>


	{{ HTML::script('//static.1.gamenadzor.ru/vendor/select2/js/select2.min.js') }}
	{{ HTML::style('//static.1.gamenadzor.ru/vendor/select2/css/select2.min.css') }}
	<script>
	$('.chosen').select2({
		tags: true,
		multiple: true
	});
	</script>
@stop