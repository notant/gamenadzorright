@extends('layouts.master')

@section('content')
	@include('gn-admin.reviews.parts.tinymce')

	{{ Form::open(array('files' => true, 'class' => 'editor-ui')) }}

		@include('gn-admin.reviews.parts.create')

	{{ Form::close() }}

@stop