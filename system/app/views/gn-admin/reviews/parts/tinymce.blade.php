{{ HTML::script('assets/ckeditor/ckeditor.js') }}
{{ HTML::script('assets/ckeditor/config.js') }}
{{ HTML::script('assets/ckeditor/styles.js') }}

{{ HTML::script('assets/jquery.mask.js') }}

<script type="text/javascript">

	$(document).ready(function() {
		CKEDITOR.replace("CKeditor", {
			filebrowserBrowseUrl: '/elfinder/ckeditor4',
			filebrowserWindowWidth: '640',
			filebrowserWindowHeight: '480'
		});

 		$('.input-time').mask('00:Z0', {
 			placeholder: '--:--',
 			translation: {
 				'Z': {
 					pattern: '[0-5]'
 				}
 			}
 		});

		$('.editor-ui').submit(function(e) {

			CKEDITOR.instances.CKeditor.updateElement();

			var FormD = new FormData( $(this)[0] ),
				LoadingModal = $("#loading-modal-window");

			LoadingModal.foundation('reveal', 'open');

			$.ajax({
				url: $(this).attr('action'),
				type: 'POST',
				data: FormD,
				async: true,
				success: function (data) {
					LoadingModal.foundation('reveal', 'close');

					if (data.redirect) {
						window.location.href = data.redirectURL;
					}
					var mw = $("#modal-window");
					mw.find('.status').text(data.status);
					mw.find('.msg').text(data.msg);
					mw.find('.url').attr('href', data.url);

					mw.foundation('reveal', 'open');

				},
				fail: function (data) {
					LoadingModal.foundation('reveal', 'close');

					var mw = $("#modal-window");
					mw.find('.status').text("Не ок");
					mw.find('.msg').text("Что-то пошло не так");
					mw.foundation('reveal', 'open');
				},
				cache: false,
				contentType: false,
				processData: false
			});

			return false;
		});
	});

</script>