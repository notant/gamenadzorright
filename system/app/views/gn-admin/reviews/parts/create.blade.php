{{ Form::hidden('editorId', Auth::user()->id) }}

<div class="large-12 panel">
	<div class="content-panel large-6 columns">
		<div class="">
			{{ Form::label('title', 'Название') }}
			{{ Form::text('title') }}

			{{ Form::label('gameName', 'Название игры') }}
			{{ Form::text('gameName') }}

			{{ Form::label('description', 'Краткое описание') }}
			{{ Form::textarea('description', null, array('size' => '30x1')) }}

			{{ Form::label('tags', 'Тэги') }}
			{{ Form::text('tags') }}

			<div class="large-6 clearfix">
				{{ Form::label('reviewScreenshot', 'Превью-скриншот') }}

				@if ($review && $review->reviewScreenshot)
					<img src="/{{ $review->reviewScreenshot }}" />
				@endif

				{{ Form::file('reviewScreenshot') }}
			</div>
		</div>
	</div>

	<div class="large-6 columns">
		<div class="row">
			<div class="review-panel large-6 columns">
				{{ Form::label('categoryId', 'Жанр') }}
				@if ($review && $review->categoryId)
					{{ Form::select('categoryId', Category::Select(), $review->categoryId) }}
				@else
					{{ Form::select('categoryId', Category::Select(), 22) }}
				@endif

				{{ Form::label('hoursPlayed', 'Времени потрачено') }}
				{{ Form::text('hoursPlayed', null, array('class' => 'input-time')) }}

				{{ Form::label('hoursWasted', 'Времени жаль') }}
				{{ Form::text('hoursWasted', null, array('class' => 'input-time')) }}


				<div class="row collapse">
					<div class="large-6 columns">
						{{ Form::label('storyScore', 'Сюжет', array('class' => 'prefix')) }}
					</div>
					<div class="large-6 columns">
						{{ Form::text('storyScore') }}
					</div>
				</div>

				<div class="row collapse">
					<div class="large-6 columns">
						{{ Form::label('controlScore', 'Управление', array('class' => 'prefix')) }}
					</div>
					<div class="large-6 columns">
						{{ Form::text('controlScore') }}
					</div>
				</div>

				<div class="row collapse">
					<div class="large-6 columns">
						{{ Form::label('graphicsScore', 'Графика', array('class' => 'prefix')) }}
					</div>
					<div class="large-6 columns">
						{{ Form::text('graphicsScore') }}
					</div>
				</div>

				<div class="row collapse">
					<div class="large-6 columns">
						{{ Form::label('soundScore', 'Звук', array('class' => 'prefix')) }}
					</div>
					<div class="large-6 columns">
						{{ Form::text('soundScore') }}
					</div>
				</div>
			</div>
			<div class="review-panel large-6 columns">
				<div class="row">
					<div class="large-6 columns">
						{{ Form::label('isSpoilers', 'Спойлеры') }}
					</div>
					<div class="large-6 columns">
						{{ Form::checkbox('isSpoilers') }}
					</div>
				</div>
				<div class="row">
					<div class="large-6 columns">
						{{ Form::label('editorsAward', 'Выбор редакции') }}
					</div>
					<div class="large-6 columns">
						{{ Form::checkbox('editorsAward') }}
					</div>
				</div>

				<div class="row">
					<div class="large-5 columns">
						{{ Form::label('isPublished', 'Статус', array('class' => 'inline')) }}
					</div>
					<div class="large-7 columns">
						@if ($review && isset($review->isPublished))
							{{ Form::select('isPublished', array(
								0 => 'Черновик',
								1 => 'Опубликовано'
								), $review->isPublished) }}
						@else
							{{ Form::select('isPublished', array(
								0 => 'Черновик',
								1 => 'Опубликовано'
							), 0) }}
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row">
		</div>
	</div>

	<div class="large-12 columns end">
		<div id="tinyMCE">
			{{ Form::label('reviewText', 'Текст обзора') }}
			{{ Form::textarea('reviewText', null, array('id' => 'CKeditor')) }}
		</div>
	</div>


	{{ Form::submit('Создать', array('class' => 'button large expand')) }}

</div>
