@extends('layouts.master')

@section('content')


	<div class="large-10 columns large-centered row">

	<h3 class="text-center">{{ 'Зайди, если хочешь продолжить' }}</h3>

	@if (!Auth::check())

		{{ Form::open() }}

		<div class="row collapse">
			<div class="large-4 columns">
				{{ Form::label('username', 'Логин', array('class' => 'prefix')) }}
			</div>
			<div class="large-8 columns">
				{{ Form::text('username', null) }}
			</div>
		</div>

		<div class="row collapse">
			<div class="large-4 columns">
				{{ Form::label('password', 'Пароль', array('class' => 'prefix')) }}
			</div>
			<div class="large-8 columns">
				{{ Form::password('password', null) }}
			</div>
		</div>

		{{ Form::submit('Войти', array('class' => 'small button')) }}

		{{ Form::close() }}

	@endif

	</div>

@stop