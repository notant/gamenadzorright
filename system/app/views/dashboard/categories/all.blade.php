@extends('......layouts.dashboard')

@section('content')

	<div class="large-12 columns dashboard-inside">
		<div class="dashboard-inside--wrapper border">
			<h3 class="dashboard-inside--head">Категории ({{ Category::count() }})</h3>
			<div class="dashboard-inside--body">
				<table class="display" cellspacing="0" width="100%" id="categories-table">
					<thead>
						<tr>
							<td>Название</td>
							<td>URL</td>
							<td>Приоритет</td>
							<td>Показывать в меню</td>
							<td>Действия</td>
						</tr>
					</thead>
					<tbody>
					@foreach(Category::all() as $category)

						<tr>
							<td>{{$category->categoryName}}</td>
							<td>{{HTML::link('/reviews/' . $category->categoryUrl, $category->categoryName)}}</td>
							<td>{{$category->categoryPriority}}</td>
							<td>{{$category->categoryShowInMenu}}</td>
							<td>
								<div class="large-6 middle-6 columns">
									{{ HTML::linkAction('Dashboard@getEditableCategory', 'Редактировать', ($category->id)) }}
								</div>

								<div class="text-right large-6 middle-6 columns end">
									{{ Form::open(
										array(
											'method' => 'DELETE',
											'action' => array('Dashboard@deleteCategory', $category->id)
										)
									)}}
									{{ Form::submit('Удалить', array('class' => 'button tiny delete-button')) }}
									{{ Form::close() }}
								</div>
							</td>
						</tr>

					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@stop

@section('head-scripts')
	{{ HTML::style('//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css') }}
	{{ HTML::style('//cdn.datatables.net/plug-ins/3cfcc339e89/integration/foundation/dataTables.foundation.css') }}
	{{ HTML::script('//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js') }}

	<style>
		.button.tiny.delete-button {
			margin-bottom: 0;
		}
	</style>

	<script>
	$(document).ready(function(){
		$('#categories-table').DataTable({
			'language': {
				'url': '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Russian.json'
			}
		});

		$('delete-button').click(function() {


			return false;
		});
	});
	</script>

@stop