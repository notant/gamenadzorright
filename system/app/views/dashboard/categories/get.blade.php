@extends('layouts.dashboard')

@section('content')
	<div class="large-12 columns dashboard-inside">
		<div class="dashboard-inside--wrapper border">
			@if ($category)
				<h1 class="dashboard-inside--head">Категория {{$category->categoryName}}</h1>
				{{ Form::model($category) }}
			@else
				<h1 class="dashboard-inside--head">Новая категория</h1>
				{{ Form::open() }}
			@endif
				<div class="dashboard-inside--body large-12 medium-12 large-centered columns end">
					<div class="large-12 clearfix">
						<div class="large-3 columns">
							{{ Form::label('categoryName', 'Название категории', array('class' => 'inline')) }}
						</div>
						<div class="large-9 columns end">
							{{ Form::text('categoryName') }}
						</div>
					</div>
					<div class="large-12 clearfix">
						<div class="large-3 columns">
							{{ Form::label('categoryPriority', 'Приоритет', array('class' => 'inline')) }}
						</div>
						<div class="large-9 columns end">
							{{ Form::text('categoryPriority') }}
						</div>
					</div>
					<div class="large-12 clearfix">
						<div class="large-3 columns">
							{{ Form::label('categoryShowInMenu', 'Показывать в меню', array('class' => 'inline')) }}
						</div>
						<div class="large-9 columns end">
							@if ($category)
								{{ Form::checkbox('categoryShowInMenu', 1, $category->categoryShowInMenu) }}
							@else
								{{ Form::checkbox('categoryShowInMenu') }}
							@endif
						</div>
					</div>
					<div class="large-6 large-centered columns end clearfix">
						@if ($category)
							{{ Form::submit('Изменить', array('class' => 'button expand')) }}
						@else
							{{ Form::submit('Добавить', array('class' => 'button expand')) }}
						@endif
					</div>
				</div>


			{{ Form::close() }}
		</div>
	</div>
@stop