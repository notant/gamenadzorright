<section class="large-3 columns" data-equalizer-watch>
	<h4>{{HTML::link('/gn-admin/dashboard', Auth::user()->username )}}</h4>
</section>

<section class="large-9 columns" data-equalizer-watch>
	<span class="right">{{ HTML::link('/logout', 'Выход') }}</span>
</section>