@extends('...layouts.dashboard')

@section('content')

	<div class="large-6 medium-6 small-12 columns">
		<div class="stats border">
			<h3 class="panel-heading">Категории ({{ Category::count() }})</h3>

			<div class="panel-body" data-equalizer-watch>
				<?php
				Paginator::setPageName('categories_page');
				$categories = Category::paginate(10); ?>
				@foreach($categories as $key => $category)
					<div class="category">
						<h5>{{ $category->categoryName }}</h5>
					</div>
				@endforeach
			</div>

			<div class="panel-footer">{{ $categories->appends('reviews_page', Input::get('reviews_page',1))->links() }}</div>
		</div>
	</div>

	<div class="large-6 medium-6 small-12 columns">
		<div class="stats border">
			<h3 class="panel-heading">Обзоры ({{ Review::count() }})</h3>

			<div class="panel-body" data-equalizer-watch>
				<?php
				Paginator::setPageName('reviews_page');
				$reviews = Review::orderBy('published_at', 'DESC')->paginate(10);?>
				@foreach($reviews as $review)
					<div class="review">
						<h5>{{ $review->title }}</h5>
					</div>
				@endforeach
			</div>

			<div class="panel-footer">{{ $reviews->appends('categories_page', Input::get('categories_page',1))->links() }}</div>
		</div>
	</div>


@stop