@extends('layouts.dashboard-clear')

@section('content')

	<div class="dashboard-form {{$errorclass}}">

		<div class="form-header">
			<h1>Вход</h1>
		</div>

		<div class="form-body">
			{{ Form::open(array('action' => 'Dashboard@postLoginUser')) }}

				<div class="row collapse">
					<div class="large-2 columns">
						{{ HTML::decode(Form::label('username', '<i class="fa fa-user"></i>', array('class' => 'prefix'))) }}
					</div>
					<div class="large-10 columns">
						{{ Form::text('username', null, array('placeholder' => 'Логин')) }}
					</div>
				</div>

				<div class="row collapse">
					<div class="large-2 columns">
						{{ HTML::decode(Form::label('password','<i class="fa fa-key"></i>', array('class' => 'prefix'))) }}
					</div>
					<div class="large-10 columns">
						{{ Form::password('password', null, array('placeholder' => 'Пароль')) }}
					</div>
				</div>

				<div class="row">
					<div class="large-offset-2 large-10">
						{{ Form::submit('Войти', array('class' => 'expand button')) }}
					</div>
				</div>
			{{ Form::close() }}

			<div class="form-footer">
				<div class="large-offset-2 large-10">
					@if ($message)
						<div class="alert alert--error">
							{{ $message }}
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>


@stop

@section('head-scripts')
	{{ HTML::script('//code.jquery.com/ui/1.11.2/jquery-ui.js') }}

	<script>
	$(document).ready(function() {
		$(".dashboard-form.form-error").effect('shake');
	})
	</script>

	<style>
		.dashboard-form {
			width: 400px;
			margin: 0 auto;
			position: fixed;
			top: 50%;
			margin-top: -155px;
			left: 50%;
			margin-left: -200px;
			height: 310px;
			border-radius: 15px;
		}

		.form-header {
			margin: 0 auto;
			text-align: center;
			height: 60px;
			background: rgb(240, 240, 240);
			border-radius: 15px 15px 0 0;
			border: 15px solid rgba(0,0,0,.5);
			border-bottom: 0;
		}
		.form-header h1 { margin: 0;
			border-radius: 15px 15px 0 0; }
		.form-body {
			padding: 25px;
			height: 250px;
			background: #f3f3f3;
			border-radius: 0 0 15px 15px;
			border: 15px solid rgba(0,0,0,.5);
			border-top: 0;
		}
		.form-footer .alert {
			font-style: italic;
			text-align: center;
		}
		.alert--error {
			color: red;
			font-size: 10px;
		}
	</style>

@stop