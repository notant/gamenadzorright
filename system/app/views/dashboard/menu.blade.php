<ul class="side-nav">
	<li class="heading">Категории</li>
	<li>
		{{ HTML::linkAction('Dashboard@getAllCategories', "Все категории") }}
	</li>
	<li>
		{{ HTML::linkAction('Dashboard@getEditableCategory', "Новая категория") }}
	</li>
	<li class="divider"></li>
	<li class="heading">Обзоры</li>
	<li>
		{{ HTML::linkAction('Dashboard@getAllReview', 'Все обзоры') }}
	</li>
	<li>
		{{ HTML::linkAction('Dashboard@getEditableReview', 'Новый обзор') }}
	</li>
	<li class="divider"></li>
	<li class="heading">Лолы</li>
	<li>
		{{ HTML::linkAction('Dashboard@getAllLols', 'Все ЛОЛы') }}
	</li>
	<li>
		{{ HTML::linkAction('Dashboard@getEditableLol', 'Создать новый ЛОЛ') }}
	</li>
	<li class="divider"></li>
	<li>
		{{ HTML::link('/logout', 'Выход') }}
	</li>
</ul>