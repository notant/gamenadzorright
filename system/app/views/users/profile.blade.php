@extends('layouts.master')

@section('content')
	@if (Auth::check())

		@if (Auth::user()->isAdmin || Auth::user()->isEditor)
			{{ HTML::link('admin/reviews/addreview', 'Создать обзор') }}
		@endif

	@endif

@stop