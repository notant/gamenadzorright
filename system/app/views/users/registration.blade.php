@extends('layouts.master')

@section('content')

	@if ($errors->all())
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
			<p>{{ $error }}</p>
			@endforeach
		</div>
	@endif

	<h1>Регистрация</h1>
	{{ Form::open(array('url' => 'users/registration', 'role' => 'form', 'files' => true)) }}

	<div class="row collapse">
		<div class="small-3 large-2 columns">
			{{ Form::label('email', 'E-Mail', array('class' => 'prefix')) }}
		</div>
		<div class="small-9 large-10 columns">
			{{ Form::email('email', null) }}
		</div>
	</div>

	<div class="row collapse">
		<div class="small-3 large-2 columns">
			{{ Form::label('username', 'Логин', array('class' => 'prefix')) }}
		</div>
		<div class="small-9 large-10 columns">
			{{ Form::text('username', null) }}
		</div>
	</div>

	<div class="row collapse">
		<div class="small-3 large-2 columns">
			{{ Form::label('password', 'Пароль', array('class' => 'prefix')) }}
		</div>
		<div class="small-9 large-10 columns">
			{{ Form::password('password') }}
		</div>
	</div>

	<div class="row collapse">
		<div class="small-3 large-2 columns">
			{{ Form::label('password_confirmation', 'Повтор пароля', array('class' => 'prefix')) }}
		</div>
		<div class="small-9 large-10 columns">
			{{ Form::password('password_confirmation') }}
		</div>
	</div>

	<div class="row collapse">
		<div class="small-3 large-2 columns">
			{{ Form::label('userimage', 'Изображение профиля', array('class' => 'prefix')) }}
		</div>
		<div class="small-9 large-10 columns">
			{{ Form::file('userimage', array('class' => '')) }}
		</div>
	</div>

	<button type="submit" class="btn btn-primary">Зарегистрироваться</button>

	{{ Form::close() }}

	</div>
@stop