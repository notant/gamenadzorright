<nav class="top-bar" data-topbar>

	<div @if(Request::path() == '/') class="home-link active" @else class="home-link" @endif>
		{{ HTML::link('/', 'Главная') }}
	</div>

	<div class="hidden-menu">
		<i class="fa fa-bars fa-2x right"></i>
	</div>

	<div class="top-bar-section left main-menu clearfix">
		<ul class="left">
		@foreach(Category::all() as $category)
			@if ($category->categoryShowInMenu)
			<?php
				$categoryUrl = 'reviews/' . $category->categoryUrl;
			?>
			<li @if(Request::path() == $categoryUrl) class="active" @endif>
				{{ HTML::link($categoryUrl, $category->categoryName) }}
			</li>
			@endif
		@endforeach
		</ul>

		<ul class="title-area right">
			@if (Auth::check())
				<li class="name">
					<h1>{{ HTML::link(URL::route('admin.profile'), Auth::user()->username) }}</h1>
				</li>
			@endif
		</ul>
	</div>

</nav>