<title>Gamenadzor.ru - Игровые обзоры | {{ $title or '' }}</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="игры, игровые обзоры, обзоры игр, gamenadzor, тактики{{{ $keywords or ', games, обзоры' }}}" />
<meta name="description" content="{{{ $desc or 'Свежие игровые обзоры. Тактики, прохождения и секреты от геймеров для геймеров.' }}}" />
@yield('meta-og')
<meta name='yandex-verification' content='7d2517a450d26549' />
<meta name="google-site-verification" content="fEHZMPSvOTyCljz9R6tRDbky1UUneKLsD6fVexwQ2RY" />
<meta name="msvalidate.01" content="DD35F8EB49EB53B7AC7527A44A268F44" />
<link rel="shortcut icon" href="//static.1.gamenadzor.ru/favicon-24x24.png" type="image/x-icon">
<link rel="icon" href="//static.1.gamenadzor.ru/favicon-24x24.png" type="image/x-icon">
<link rel="icon" type="image/png" href="//static.1.gamenadzor.ru/favicon-96x96.png" sizes="96x96 32x32 16x16 64x64" />
<link rel="publisher" href="https://plus.google.com/+GamenadzorRuOfficial" />

<?php
echo HTML::style('//static.1.gamenadzor.ru/foundation/css/normalize.css');
echo HTML::style('//static.1.gamenadzor.ru/foundation/css/foundation.min.css');
echo HTML::style('//static.1.gamenadzor.ru/master.css?ver=1');
echo HTML::script('//static.1.gamenadzor.ru/foundation/js/vendor/jquery.js');
echo HTML::style('//static.1.gamenadzor.ru/3/socicon.css');
?>
<!--[if lt IE 9]>
	{{ HTML::script('http://css3-mediaqueries-js.googlecode.com/files/css3-mediaqueries.js') }}
	{{ HTML::script('http://html5shim.googlecode.com/svn/trunk/html5.js') }}
<![endif]-->

@yield('head-scripts')