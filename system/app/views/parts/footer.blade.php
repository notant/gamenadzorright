<div class="clearfix">
	<div class="center-row large-12 clearfix">
		<gcse:searchbox-only></gcse:searchbox-only>
	</div>
	<div class="center-row clearfix">
		<div class="large-3 medium-6 columns">
			<h3 class="footer-reviews-header"><a href="/reviews/">Последние обзоры</a></h3>
			<ul class="last-review actual-reviews">
				@foreach($link_list as $link)
				<li>
					{{ link_to($link['url'], $link['title']) }}
				</li>
				@endforeach
			</ul>
		</div>

		<div class="large-3 medium-6 columns">
			<h3 class="footer-reviews-header"><a href="/reviews/miniobzory">Мини-обзоры</a></h3>
			<ul class="mini-reviews actual-reviews">
				@foreach($link_minis as $link)
				<li>
					{{ link_to($link['url'], $link['title']) }}
				</li>
				@endforeach
			</ul>
		</div>
		<div class="large-6 medium-12 columns future-reviews">
			<h3 class="footer-reviews-header"><span>Скоро</span></h3>
			<ul>
				<li><span>Литейная клана Чёрной горы</span></li>
				<li><span>Dragon Age: Inquisition</span></li>
				<li><span>Deus Ex: Human Revolution</span></li>
				<li><span>The Witcher</span></li>
				<li><span>Немного о кликерах: Tangerine Tycoon</span></li>
			</ul>
		</div>

		<div class="large-6 medium-12 columns"></div>
	</div>
</div>
<div class="clearfix">
</div>

<div class="center-row clearfix">
	<div class="large-6 medium-6 small-6 columns text-left">
		<span class="copyright">gamenadzor.ru &copy; {{ date('Y') }}</span>
		<span class="info-email">Для связи: <a href="mailto:public@gamenadzor.ru">public@gamenadzor.ru</a></span>
	</div>
	<div class="large-6 medium-6 small-6 columns text-right social">
		<a href="http://vk.com/gamenadzor" class="socicon-icon"><span class="socicon socicon-vkontakte"></span></a>
		<a href="https://plus.google.com/+GamenadzorRuOfficial" class="socicon-icon"><span class="socicon socicon-google"></span></a>
		<a href="http://twitter.com/gamenadzor" class="socicon-icon"><span class="socicon socicon-twitter"></span></a>
	</div>
</div>

<noindex>
	<div id="modal-window" class="tiny reveal-modal" data-reveal>
	  <h2 class="status"></h2>
	  <p class="msg"></p>
	  <a href="#" class="url">К обзору!</a>

	  <a class="close-reveal-modal">&#215;</a>
	</div>

	<div id="loading-modal-window" class="tiny reveal-modal" data-reveal>
		<!-- 5 -->
		<div class="loader" title="4">
			<svg width='124px' height='124px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-cube">
			  <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
			  <g transform="translate(25 25)">
				<rect x="-20" y="-20" width="40" height="40" fill="#081E2F" opacity="0.9" class="cube">
				  <animateTransform attributeName="transform" type="scale" from="1.5" to="1" repeatCount="indefinite" begin="0s" dur="1s" calcMode="spline" keySplines="0.2 0.8 0.2 0.8" keyTimes="0;1"></animateTransform>
				</rect>
			  </g>
			  <g transform="translate(75 25)">
				<rect x="-20" y="-20" width="40" height="40" fill="#081E2F" opacity="0.8" class="cube">
				  <animateTransform attributeName="transform" type="scale" from="1.5" to="1" repeatCount="indefinite" begin="0.1s" dur="1s" calcMode="spline" keySplines="0.2 0.8 0.2 0.8" keyTimes="0;1"></animateTransform>
				</rect>
			  </g>
			  <g transform="translate(25 75)">
				<rect x="-20" y="-20" width="40" height="40" fill="#081E2F" opacity="0.7" class="cube">
				  <animateTransform attributeName="transform" type="scale" from="1.5" to="1" repeatCount="indefinite" begin="0.3s" dur="1s" calcMode="spline" keySplines="0.2 0.8 0.2 0.8" keyTimes="0;1"></animateTransform>
				</rect>
			  </g>
			  <g transform="translate(75 75)">
				<rect x="-20" y="-20" width="40" height="40" fill="#081E2F" opacity="0.6" class="cube">
				  <animateTransform attributeName="transform" type="scale" from="1.5" to="1" repeatCount="indefinite" begin="0.2s" dur="1s" calcMode="spline" keySplines="0.2 0.8 0.2 0.8" keyTimes="0;1"></animateTransform>
				</rect>
			  </g>
			</svg>
		</div>
	</div>
</noindex>

@include('parts.footernoindexmaster')

<div class="right gamenadzor-big-sign large-12 medium-12 small-12 text-right">
	<img src="//static.1.gamenadzor.ru/gamenadzor-big-sign.png" alt="gamenadzor.ru" title="gamenadzor.ru" />
</div>
