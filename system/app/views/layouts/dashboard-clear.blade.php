<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
	@include('...parts.head')
</head>
<body>
<div class="large-12 dashboard main-container">

	<div class="clear"></div>
	<div class="content dashboard-body clearfix">
		<div class="large-12 small-12 medium-12 columns content-column" data-equalizer>
			@yield('content')
		</div>
	</div>
</div>

@include('...parts.scripts')

</body>
</html>