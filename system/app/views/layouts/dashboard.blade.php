<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
	@include('...parts.head')
</head>
<body>
<div class="large-12 dashboard main-container">

	<div class="clear"></div>

	<header class="large-12 dashboard-head clearfix" data-equalizer>
		@include('...dashboard.header')
	</header>

	<div class="content dashboard-body clearfix">
		<div class="large-3 small-3 medium-3 columns menu-column">
			@include('...dashboard.menu')
		</div>
		<div class="large-9 small-9 medium-9 columns content-column" data-equalizer>
			@yield('content')
		</div>
	</div>
</div>

@include('...parts.scripts')

</body>
</html>