<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
	@include('...parts.head')
</head>
<body>
<!-- BEGIN SiteCTRL Script -->
<script type="text/javascript">
	if(document.location.protocol=='http:'){
		var Tynt=Tynt||[];Tynt.push('a_LgTCIuar5lwLacwqm_6l');
		(function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
	}
</script>
<!-- END SiteCTRL Script -->
<div class="large-12 main-container">
	<header id="header">
		@include('...parts.header')
	</header>

	<div class="clear"></div>

	<div class="content" id="container">
		<div class="large-12 clearfix {{ $pageClass or 'master' }}">
			@yield('content')
		</div>
	</div>
</div>

<footer id="footer" class="large-12 clearfix">
	@include('...parts.footer')
</footer>

@include('...parts.scripts')

</body>
</html>