@extends('layouts.master')

@section('content')

	<?php $first = $reviews[0];//$reviews->remember(60, 'main_review')->get()->first();?>

	<div class="main-review-panel" style="background: black url('{{ $first->reviewScreenshot }}') no-repeat 50% 50%">

		<a class="review-link" href="{{
					URL::action(
						'CReviews@getReview',
						array(
							'category' => $first->category['categoryUrl'],
							'review' => $first->reviewUrl
						)
					) }}" >
			<div class="review-panel">
				<div class="flags-block">
					@if ($first->user->profilename == 'gamenadzor') <img src="/upload/system/newall.png" height="60" alt="Обзор от Gamenadzor" /> @endif
					@if ($first->editorsAward) <img src="/upload/system/newcho.png" height="60" alt="Выбор редакции" /> @endif
					@if ($first->isSpoilers) <img src="/upload/system/newspo.png" height="60" alt="Спойлеры" /> @endif
				</div>
				<div class="user-image">
					@if ($first->user['userimage'])
						{{ HTML::image($first->user['userimage'], $first->user['profilename']) }}
					@endif
				</div>

				<div class="review-i">
					<div class="review-game">
						{{ $first->gameName }}
					</div>
					<div class="review-title">
						<h2>{{ $first->title }}</h2>
					</div>

					<div class="review-description">
						{{ $first->description }}
					</div>
				</div>
				<div class="review-a">
					<div class="review-author">
						{{ $first->user->profilename }}
					</div>
				</div>
			</div>
		</a>
	</div>

	<?php
	unset($reviews[0]);
	$last = $reviews;
	?>

	<ul class="mini-reviews-panel slick-carousel">
		@foreach($last as $l)
			<li>
				<a class="review-link"
				   href="{{
					URL::action(
						'CReviews@getReview',
						array(
							'category' => $l->category['categoryUrl'],
							'review' => $l->reviewUrl
						)
					) }}" >
					<div class="mini-review">
						<div class="review-panel">
							<div class="flags-block">
								@if ($l->user->profilename == 'gamenadzor') <img src="/upload/system/newall.png" height="60" alt="Обзор от Gamenadzor" /> @endif
								@if ($l->editorsAward) <img src="/upload/system/newcho.png" height="40" alt="Выбор редакции" /> @endif
								@if ($l->isSpoilers) <img src="/upload/system/newspo.png" height="40" alt="Спойлеры" /> @endif
							</div>
							<div class="mini-review-image">
								<img src="{{ $l->thumbScreenshot }}" width="280px" alt="{{ $l->title }}"/>
							</div>
							<div class="mini-review-info">
								<div class="mini-review-title">{{ $l->gameName }}</div>
								<div class="mini-review-desc">{{ $l->title }}</div>
							</div>
						</div>
					</div>
				</a>
			</li>
		@endforeach

	</ul>

@stop

@section('head-scripts')

	{{ HTML::script('//static.1.gamenadzor.ru/vendor/slick/slick.min.js') }}
	{{ HTML::style('//static.1.gamenadzor.ru/vendor/slick/slick.css') }}

	<style>
	ul.slick-carousel {
		margin-left: 0;
		padding: 0 25px;
		position: relative;
	}
	.slider-button {
		position: absolute;
		color: white;
		font-size: 20px;
		height: 150px;
		width: 25px;
		top: 0;
		transition: all 0.5s;
		text-align: center;
	}
	.slider-button:hover {
		cursor: pointer;
	}
	.slider-button--prev {
		left: 0;
	}
	.slider-button--next {
		right: 0;
	}
	.slider-button > .slider-icon {
		line-height: 150px;
		transition: all 0.5s;
	}
	.slider-button:hover > .slider-icon {
		color: rgb(165, 46, 16);
	}
	.slick-slide .flags-block img {
		display: inline-block;
	}
	.mini-reviews-panel {
		opacity: 0;
	}
	</style>

	<script>
		$(document).ready(function() {
			var $slick = $('.slick-carousel');

			$slick.animate({'opacity': 1}, 500);

			$slick.slick({
				infinite: true,
				speed: 2000,
				slidesToShow: 4,
				slidesToScroll: 1,
				fade: false,
				slide: 'li',
				autoplay: true,
				autoplaySpeed: 5000,
				responsive: [
					{
						breakpoint: 1200,
						settings: {
							arrows: true,
							slidesToShow: 3,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 950,
						settings: {
							arrows: true,
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 650,
						settings: {
							arrows: true,
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				],
				prevArrow: '<div class="slider-button slider-button--prev"><i class="slider-icon fa fa-chevron-left"></i></div>',
				nextArrow: '<div class="slider-button slider-button--next"><i class="slider-icon fa fa-chevron-right"></i></div>'
			})
		});
	</script>

@stop