@extends('layouts.master')

@section('meta-og')
	<meta property="og:description" content="{{ $review->description }}" />
@stop

@section('content')

	<article class="review">
		<div class="article-head clearfix">

			<div class="review-head large-6">
				<h1>{{ $review->title }}</h1>
			</div>

			<div class="review-flags-block">
				@if ($review->user->profilename == 'gamenadzor') <img src="/upload/system/newall.png" height="60" alt="Обзор от Gamenadzor" /> @endif
				@if ($review->editorsAward) <img src="/upload/system/newcho.png" height="60" alt="Выбор редакции" /> @endif
				@if ($review->isSpoilers) <img src="/upload/system/newspo.png" height="60" alt="Спойлеры" /> @endif
			</div>

			<div class="review-score">
				<div class="score">
					<div class="hours-wrapper">
						<h4>Интерес</h4>
						<div class="hours-score">{{ $review->hoursScore }} % </div>
						<div class="hours-percent-line">
							<span style="width: {{$review->hoursScore}}%"></span>
						</div>

						<div class="hours-description">
							<div class="hours-played">{{ $review->hoursPlayed }}</div>
							<div class="hours-wasted">{{ $review->hoursWasted }}</div>
						</div>

					</div>

					<div class="score-wrapper">

						@if ($review->graphicsScore > 0)
						<div class="score-line">Графика: <span class="score-number">{{ $review->graphicsScore }}</span></div>
						@endif
						@if ($review->soundScore > 0)
						<div class="score-line">Звук: <span class="score-number">{{ $review->soundScore }}</span></div>
						@endif
						@if ($review->storyScore > 0)
						<div class="score-line">Сюжет: <span class="score-number">{{ $review->storyScore }}</span></div>
						@endif
						@if ($review->controlScore > 0)
						<div class="score-line">Управление: <span class="score-number">{{ $review->controlScore }}</span></div>
						@endif

					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="review-body">
				{{ $review->reviewText }}
			</div>
		</div>

		<div class="review-footer large-12">
			<div class="review-footer-info">
				<span class="review-count">Просмотров: {{ $review->view_count }}</span>
				<span class="review-author">Автор: {{ $review->user->profilename }}</span>
			</div>
			<span class="review-tags">
				@foreach($tags as $tag)
					<a href="{{
						URL::action(
							'search.index',
							array(
								'q' => trim($tag)
							)
						) }}" class="review-tag">{{ $tag }}</a>
				@endforeach
			</span>
		</div>
		<div class="button--up left">
			Наверх <i class="fa fa-chevron-up"></i>
		</div>
	</article>

@stop

@section('head-scripts')
	<style>
		.button--up {
			background: rgb(8, 30, 47);
			display: inline-block;
			line-height: 40px;
			text-align: center;
			transition: all 0.4s;
			box-shadow: 0 1px 1px;
			height: 40px;
			width: 100%;
			font-size: 20px;
			color: #3d9eba;
			border-radius: 0 0 5px 5px;
			margin-bottom: 15px;
		}
		.button--up:hover {
			cursor: pointer;
			background: lightgray;
			color: rgb(8, 30, 47);
			border-color: #3d9eba;
		}
	</style>

	<script>
		$(document).ready(function() {
			$buttonUp = $(".button--up");

			$buttonUp.click(function() {
				$('body, html').animate({scrollTop: 0}, 1000);
			});
		});
	</script>
@stop