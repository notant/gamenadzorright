@extends('layouts.master')

@section('content')
	<div class="container">
		@foreach ($reviews as $review)
			<a class="review-list-link large-12"
				href="{{URL::action(
							'CReviews@getReview', array(
								'category' => $review->category['categoryUrl'],
								'review' => $review->reviewUrl
							))
						}}">
				<div class="review-list-wrapper clearfix">
					<div class="review-list-image">
						<img src="/{{ $review->thumbScreenshot }}" width="280px" alt="{{ $review->title }}" />
					</div>
					<div class="review-list-body small-12">
						<div class="flags-block">
							@if ($review->user->profilename == 'gamenadzor') <img src="/upload/system/newall.png" height="60" alt="Обзор от Gamenadzor" /> @endif
							@if ($review->editorsAward) <img src="/upload/system/newcho.png" height="50" alt="Выбор редакции" /> @endif
							@if ($review->isSpoilers) <img src="/upload/system/newspo.png" height="50" alt="Спойлеры" /> @endif
						</div>
						<div class="review-hours-score">
							{{ $review->hoursScore }}%
						</div>
						<div class="review-list-title"><h3>{{ $review->title }}</h3></div>
						<div class="review-list-desc">{{ $review->description }}</div>
						<div class="review-list-info">
							<span class="review-list-count">Просмотров: {{ $review->view_count }}</span>
							<span class="review-list-author">
								Автор: {{ $review->user->profilename }}
							</span>
						</div>
					</div>
				</div>
			</a>
		@endforeach
	</div>

	<?php echo $reviews->links(); ?>
@stop