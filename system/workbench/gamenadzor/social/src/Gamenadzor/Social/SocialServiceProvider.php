<?php namespace Gamenadzor\Social;

use Gamenadzor\Social\Facade\SocialFacade;
use Illuminate\Support\ServiceProvider;

class SocialServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('gamenadzor/social');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// Register 'social' instance container to our social object
		$this->app->bind('Social', function()
		{
			$config = array(
				'app_id' => 4694622,
				'scopes' => array('groups','wall','offline','photos'),
				'redirect_uri' => 'http://oauth.vk.com/blank.html',
				'response_type' => 'token',
				'secret' => 'ealJQXOywP2pKBKceJsU'
			);
			return new \Gamenadzor\Social\Social($config);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
