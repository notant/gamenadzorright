<?php namespace Gamenadzor\Social\Facade;

use Illuminate\Support\Facades\Facade;

class SocialFacade extends Facade {

	protected static function getFacadeAccessor() { return 'Social'; }

}